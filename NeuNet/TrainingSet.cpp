/*
 * TrainingSet.cpp
 *
 *  Created on: Sep. 23, 2020
 *      Author: Matthew Virdaeus
 */

#include "TrainingSet.h"

TrainingSet::TrainingSet() {
	// TODO Auto-generated constructor stub

}

TrainingSet::~TrainingSet() {
	// TODO Auto-generated destructor stub
}

void TrainingSet::addPair(const Activation &x, const Activation &y) {
	trainingPairs_.push_back(std::make_pair(x, y));
}

unsigned int TrainingSet::size() const {
	return trainingPairs_.size();
}

Activation TrainingSet::getInput(unsigned int index) const {
	return trainingPairs_.at(index).first;
}

Activation TrainingSet::getOutput(unsigned int index) const {
	return trainingPairs_.at(index).second;
}

//Splits a number of training pairs off the back of the set to create a new training set.
TrainingSet TrainingSet::split(unsigned int items) {
	TrainingSet back;
	for(auto it = trainingPairs_.end() - items; it != trainingPairs_.end(); ++it) {
		back.trainingPairs_.push_back(*it);
	}
	trainingPairs_.erase(trainingPairs_.end() - items, trainingPairs_.end());
	return back;
}
