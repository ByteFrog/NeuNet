# GateTest

A test application for the NeuNet library that randomly generates a set of XOR gates to train against and identify.

# Requirements

NeuNet library (from this project)

# How to Use

Just make sure the NeuNet library is linked.