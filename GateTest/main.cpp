/*
 * main.cpp
 *
 *  Created on: Sep. 18, 2020
 *      Author: Matthew Virdaeus
 */

#include <ctgmath>
#include <iostream>

#include "gate_generators.h"
#include "Activation.h"
#include "NeuralNet.h"

using namespace std;

vector<unsigned int>  verifyOutput(TrainingSet verificationSet, NeuralNet neuralNet) {
	vector<unsigned int> idMap;
	unsigned int correct = 0;
	for(unsigned int i = 0; i < verificationSet.size(); ++i) {
		Activation output = neuralNet.feedForward(verificationSet.getInput(i));
		if(roundf(output(0)) == verificationSet.getOutput(i)(0)) {
			++correct;
			if((i+1) % 10 == 0) {
				idMap.push_back(correct);
				correct = 0;
			}
		}
	}

	return idMap;
}

int main(int argc, char** argv) {

	vector<unsigned int> topology;
	topology.push_back(2);
	topology.push_back(3);
	topology.push_back(1);

	NeuralNet neuralNet(topology);

	TrainingSet trainingSet = generateTrainingSet(Gate::XOR, 1000);
	TrainingSet verificationSet = generateTrainingSet(Gate::XOR, 100);

	unsigned int correct = 0;
	vector<unsigned int> idMap;

	idMap = verifyOutput(verificationSet, neuralNet);
	for(auto j : idMap) {
		correct += j;
	}
	cout << "Epoch 0: " << correct << " / " << verificationSet.size();
	cout << " (";
	for(auto j : idMap) {
		cout << j << " ";
	}
	cout << ")" << endl;

	for(unsigned int i = 1; i <= 30; ++i) {
		neuralNet.train(trainingSet, 1, 10, 2.0);
		idMap = verifyOutput(verificationSet, neuralNet);
		correct = 0;
		for(auto j : idMap) {
			correct += j;
		}
		cout << "Epoch " << i << ": " << correct << " / " << verificationSet.size();
		cout << " (";
		for(auto j : idMap) {
			cout << j << " ";
		}
		cout << ")" << endl;
	}

	return 0;
}
