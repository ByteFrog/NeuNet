/*
 * NeuralNet.cpp
 *
 *  Created on: Sep. 18, 2020
 *      Author: Matthew Virdaeus
 *
 *  Notes:
 *  - I'm not usually a fan of single-letter variables in programming with a couple minor exceptions (eg. i,
 *  j, k for simple counters), but in the interest of clarity, I've used variable names that match the
 *  literature and mathematical models for artificial neural networks. This includes:
 *    * x - an input set of activations to the network
 *    * y - an expected output set of activations from the network
 *    * z - the result of the equation "wa + b" (before it's fed into the activation function)
 */

#include "NeuralNet.h"

#include <algorithm>
#include <ctgmath>
#include <chrono>
#include <iostream>

#include "sigmoids.h"

using std::vector;
using std::ptr_fun;
using Eigen::VectorXf;
using Eigen::MatrixXf;

NeuralNet::NeuralNet(const std::vector<unsigned int>& topology)
: numberGenerator_(std::chrono::system_clock::now().time_since_epoch().count()){

	/* The inputs to the neural net don't need biases, so we don't need to push a vector of biases for the
	 * first layer.
	 */

	for(vector<unsigned int>::size_type i = 1; i < topology.size(); ++i) {
		biases_.push_back(VectorXf::Random(topology.at(i)));

		float stdDeviation = 1.0 / sqrtf(topology.at(i - 1));
		std::normal_distribution<float> distribution(0, stdDeviation);

		MatrixXf matrix(topology.at(i), topology.at(i - 1));
		for(unsigned int y = 0; y < matrix.rows(); ++y) {
			for(unsigned int x = 0; x < matrix.cols(); ++x) {
				matrix(y, x) = distribution(numberGenerator_);
			}
		}
		weights_.push_back(matrix);
	}
}

NeuralNet::~NeuralNet() {
}

Activation NeuralNet::feedForward(const Activation &input) {
	auto bi = biases_.begin();
	auto wi = weights_.begin();
	VectorXf a = input.vector_;

	//The vector of biases and weights will always be the same size, so we only need to check the bounds of one of them.
	while(bi != biases_.end()) {
		a = ((*wi) * a + (*bi)).unaryExpr(ptr_fun(activationFunction));
		++bi, ++wi;
	}

	return Activation(a);
}

/* Trains the neural network using mini-batch stochastic gradient descent.
 */
void NeuralNet::train(const TrainingSet& trainingSet, unsigned int epochs, unsigned int batchSize, float learningRate) {

	vector<unsigned int> indices;
	for(unsigned int i = 0; i < trainingSet.size(); ++i) {
		indices.push_back(i);
	}

	for(unsigned int epoch = 0; epoch < epochs; ++epoch) {
		std::shuffle(indices.begin(), indices.end(), numberGenerator_);

		auto it = indices.begin();
		while(it != indices.end()) {
			vector<unsigned int> batchIndices;
			for(unsigned int i = 0; i < batchSize && it != indices.end(); ++i) {
				batchIndices.push_back(*it);
				++it;
			}

			processMinibatch(trainingSet, batchIndices, learningRate);
		}
	}

}

float NeuralNet::activationFunction(float z) {
	//return tanhf(z);
	return logistic(z);
}

float NeuralNet::activationPrimeFunction(float z) {
	//return tanh_prime(z);
	return logistic_prime(z);
}

Eigen::VectorXf NeuralNet::costDerivative(const Eigen::VectorXf &output, const Eigen::VectorXf &y) {
	return output - y;
}

/* Uses the back propagation algorithm to calculate the gradient for the cost function
 * of the neural network according to an input and expected output (x and y).
 * nabla_b and nabla_w are the gradients for biases and weights respectively.
 */

void NeuralNet::calcCostGradient(const Activation &x, const Activation &y, std::vector<Eigen::VectorXf> &nabla_b,
		std::vector<Eigen::MatrixXf> &nabla_w) const {

	nabla_b.clear();
	nabla_w.clear();

	for(VectorXf b : biases_) {
		nabla_b.push_back(VectorXf::Zero(b.rows()));
	}

	for(MatrixXf w : weights_) {
		nabla_w.push_back(MatrixXf::Zero(w.rows(), w.cols()));
	}

	VectorXf a = x.vector_;
	vector<VectorXf> activations;
	activations.push_back(a);
	vector<VectorXf> z_vectors;

	//Feed forward
	auto bi = biases_.begin();
	auto wi = weights_.begin();
	while(bi != biases_.end()) {
		VectorXf z = (*wi) * a + (*bi);
		z_vectors.push_back(z);
		a = z.unaryExpr(ptr_fun(activationFunction));
		activations.push_back(a);
		++bi, ++wi;
	}

	//Backward pass
	VectorXf delta = costDerivative(activations.back(), y.vector_).array() *
			z_vectors.back().unaryExpr(ptr_fun(activationPrimeFunction)).array();
	nabla_b.back() = delta;
	nabla_w.back() = delta * activations.at(activations.size() - 2).transpose();

	/* Unlike feed forward, an iterator here will be inconvenient because we'd need one
	 * for several containers. An index can be shared between all of them though (it's
	 * the same offset from the end, because they're all the same size.)
	 *
	 * The book this NN is based on uses the number of layers as the limit. Using the size
	 * of the activations vector works however because the bias vector size is always one
	 * less than the number of layers in the network, so pushing the input activation (x)
	 * first brings the size up to the same as the number of layers.
	 */
	for(vector<VectorXf>::size_type i = 2; i < activations.size(); ++i) {
		VectorXf z = z_vectors.at(z_vectors.size() - i);
		VectorXf sp = z.unaryExpr(ptr_fun(activationPrimeFunction));
		delta = (weights_.at(weights_.size() - i + 1).transpose() * delta).array() * sp.array();
		nabla_b.at(nabla_b.size() - i) = delta;
		nabla_w.at(nabla_w.size() - i) = delta * activations.at(activations.size() - i - 1).transpose();
	}
}

void NeuralNet::processMinibatch(const TrainingSet &trainingSet,
		const std::vector<unsigned int> &indices, float learningRate) {
	vector<VectorXf> nabla_b;
	vector<MatrixXf> nabla_w;

	for(VectorXf b : biases_) {
		nabla_b.push_back(VectorXf::Zero(b.rows()));
	}
	for(MatrixXf w : weights_) {
		nabla_w.push_back(MatrixXf::Zero(w.rows(), w.cols()));
	}

	for(unsigned int i : indices) {
		vector<VectorXf> deltaNabla_b;
		vector<MatrixXf> deltaNabla_w;

		calcCostGradient(trainingSet.getInput(i), trainingSet.getOutput(i), deltaNabla_b, deltaNabla_w);

		//The vector of weights and biases is always the same size, and the delta versions are always
		//the same size as the non-delta versions, so only one check is necessary for all of them.
		for(vector<VectorXf>::size_type j = 0; j < nabla_b.size(); ++j) {
			nabla_b.at(j) += deltaNabla_b.at(j);
			nabla_w.at(j) += deltaNabla_w.at(j);
		}
	}

	for(unsigned int i = 0; i < biases_.size(); ++i) {
		biases_.at(i) -= (learningRate / indices.size()) * nabla_b.at(i);
		weights_.at(i) -= (learningRate / indices.size()) * nabla_w.at(i);
	}
}
