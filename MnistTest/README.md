# MnistTest

A test application for the NeuNet library that identifies handwritten digits from the MNIST data set.

# Requirements

NeuNet library (from this project)

# How to Use

Link to the NeuNet library. You'll alsp need to make sure the MNIST data is in the same folder that the program will run from, as there's currently no way to specify the location of the required files. They must be decompressed and their names must be the default (eg. train-images.idx3-ubyte).