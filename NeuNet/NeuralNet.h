/*
 * NeuralNet.h
 *
 *  Created on: Sep. 18, 2020
 *      Author: Matthew Virdaeus
 *
 *  An implementation of an artificial neural network using stochastic gradient descent for learning and
 *  the logistic function for activation.
 *
 *  Notes:
 *  - This class is based on the approaches used in "Neural Networks and Deep Learning" by Michael Nielsen,
 *    specifically the code addressed in chapters 1 and 2.
 *  - This code is not optimized and despite being written in C++, often runs slowly for large data sets.
 *    (Using a mini-batch size of 10, it takes ~73s to process 50,000 images for training.)
 *
 *  Known bugs:
 *  - Although sigmoids.h includes a derivative for the tanh() function, using them in this neural network
 *    seems to prevent it from learning. It will typically start at a success rate of around 10% and
 *    make small adjustments in a seemingly random direction for the first 1 or 2 epochs, then gets stuck
 *    identifying the same images without any improvement. I'm not sure why this happens when using the logistic
 *    function seems to jump up to 90% accuracy after the first epoch before typically drifting up toward
 *    around 95% by epoch 30.
 *
 *  Potential improvements:
 *  - Implement cross-entropy cost function
 *  - Implement batch training by using matrices containing multiple activation sets (batches)
 *  - Reduce overfitting by implementing regularization
 *  - Reduce overfitting by implementing dropout
 *
 */

#pragma once

#include "Activation.h"
#include "TrainingSet.h"

#include <eigen3/Eigen/Eigen>
#include <random>
#include <utility>
#include <vector>

class NeuralNet {
public:
	NeuralNet(const std::vector<unsigned int>& topology);
	virtual ~NeuralNet();

	Activation feedForward(const Activation& input);
	void train(const TrainingSet& trainingSet, unsigned int epochs, unsigned int batchSize, float learningRate);

private:
	std::vector<Eigen::VectorXf> biases_;
	std::vector<Eigen::MatrixXf> weights_;
	std::default_random_engine numberGenerator_;

	static float activationFunction(float z);
	static float activationPrimeFunction(float z);
	static Eigen::VectorXf costDerivative(const Eigen::VectorXf& output, const Eigen::VectorXf& y);
	void calcCostGradient(const Activation& x, const Activation& y, std::vector<Eigen::VectorXf>& nabla_b,
			std::vector<Eigen::MatrixXf>& nabla_w) const;
	void processMinibatch(const TrainingSet& trainingSet, const std::vector<unsigned int>& indices, float learningRate);
};

