/*
 * main.cpp
 *
 *  Created on: Sep. 27, 2020
 *      Author: Matthew Virdaeus
 *
 *  Loads data from the MNIST data set and trains a neural network to recognize the digits contained in it.
 */

#include <arpa/inet.h>	//htonl()
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "NeuralNet.h"

using namespace std;

bool loadData(string trainingImagesPath, string trainingLabelsPath, TrainingSet& trainingSet) {
	uint32_t magicNumber; //Used to verify file integrity.

	ifstream trainingImages(trainingImagesPath, ios_base::binary);
	if(trainingImages.is_open()) {
		trainingImages.read(reinterpret_cast<char*>(&magicNumber), sizeof(magicNumber));
		magicNumber = htonl(magicNumber);
		if(magicNumber != 2051) {
			cerr << "Error: " << trainingImagesPath << ". Incorrect header." << endl;
			trainingImages.close();
		}
	}
	else {
		cerr << "Error: Failed to open " << trainingImagesPath << endl;
	}

	ifstream trainingLabels(trainingLabelsPath, ios_base::binary);
	if(trainingLabels.is_open()) {
		trainingLabels.read(reinterpret_cast<char*>(&magicNumber), sizeof(magicNumber));
		magicNumber = htonl(magicNumber);
		if(magicNumber != 2049) {
			cerr << "Error: " << trainingLabelsPath << ". Incorrect header." << endl;
			trainingLabels.close();
		}
	}
	else {
		cerr << "Error: Failed to open " << trainingLabelsPath << endl;
	}

	if(!trainingImages.is_open() || !trainingLabels.is_open()) {
		cerr << "Failed to open one or more training files. Aborting." << endl;
		trainingImages.close();
		trainingLabels.close();
		return false;
	}

	uint32_t imageCount = 0;
	uint32_t labelCount = 0;

	trainingImages.read(reinterpret_cast<char*>(&imageCount), sizeof(imageCount));
	trainingLabels.read(reinterpret_cast<char*>(&labelCount), sizeof(labelCount));
	imageCount = htonl(imageCount);
	labelCount = htonl(labelCount);
	if(imageCount != labelCount) {
		cerr << "Error: Training and label and image counts do not match. Aborting." << endl;
		trainingImages.close();
		trainingLabels.close();
		return false;
	}

	//Since this isn't a general-use program (ie. it's only for the MNIST set), we won't bother
	//checking the rows and columns values. Assume they're correct.
	trainingImages.seekg(16, ios::beg);
	trainingLabels.seekg(8, ios::beg);

	unsigned int itemCount = 0;
	while(!trainingImages.eof() && !trainingLabels.eof() && itemCount < imageCount) {
		Activation input(784);
		Activation output(10);

		for(unsigned int i = 0; i < 784; ++i) {
			char byte;
			trainingImages.get(byte);
			input(i) = ((unsigned char)byte) / 255.0;
			//input(i) = (((unsigned char)byte) * 2 / 255.0) - 1;
		}
		char byte;
		trainingLabels.get(byte);
		output((unsigned int)byte) = 1.0;

		trainingSet.addPair(input, output);

		if(++itemCount % 5000 == 0) {
			cout << "Loaded " << itemCount << " items." << endl;
		}
	}

	trainingImages.close();
	trainingLabels.close();

	cout << "Loading complete. " << itemCount << " items loaded." << endl;

	return true;
}

unsigned int verifyOutput(TrainingSet verificationSet, NeuralNet neuralNet) {
	unsigned int correct = 0;
	for(unsigned int i = 0; i < verificationSet.size(); ++i) {
		Activation output = neuralNet.feedForward(verificationSet.getInput(i));
		if(output.maxIndex() == verificationSet.getOutput(i).maxIndex()) {
			++correct;
		}
	}
	return correct;
}

int main(int argc, char** argv) {

	cout << "Loading training data..." << endl;
	TrainingSet trainingSet;
	if(!loadData("train-images.idx3-ubyte", "train-labels.idx1-ubyte", trainingSet)) {
		return 1;
	}
	cout << "Training set loaded." << endl;

	cout << "Loading verification data..." << endl;
	TrainingSet testSet;
	if(!loadData("t10k-images.idx3-ubyte", "t10k-labels.idx1-ubyte", testSet)) {
		return 1;
	}
	cout << "Verification set loaded." << endl;


	cout << "Training neural network..." << endl;

	vector<unsigned int> topology;
	topology.push_back(784);
	topology.push_back(30);
	topology.push_back(10);

	NeuralNet neuralNet(topology);
	unsigned int correct = 0;

	std::chrono::system_clock::time_point startTime, endTime;
	unsigned int trainTime, verifyTime;

	startTime = std::chrono::system_clock::now();
	correct = verifyOutput(testSet, neuralNet);
	endTime = std::chrono::system_clock::now();
	verifyTime = std::chrono::duration_cast<std::chrono::seconds>(endTime - startTime).count();
	cout << "Epoch 0: " << correct << "/" << testSet.size() << " (verify:" << verifyTime << "s)" << endl;

	/* To better see if our neural net is learning, we'll only do one epoch at a time before
	 * checking against the training set. So we'll do 30 epochs, but in a for-loop instead of
	 * through the train() method.
	 */

	for(unsigned int i = 1; i <= 30; ++i) {
		startTime = std::chrono::system_clock::now();
		neuralNet.train(trainingSet, 1, 10, 3.0);
		endTime = std::chrono::system_clock::now();
		trainTime = std::chrono::duration_cast<std::chrono::seconds>(endTime - startTime).count();
		startTime = std::chrono::system_clock::now();
		correct = verifyOutput(testSet, neuralNet);
		endTime = std::chrono::system_clock::now();
		verifyTime = std::chrono::duration_cast<std::chrono::seconds>(endTime - startTime).count();
		cout << "Epoch " << i << ": " << correct << "/" << testSet.size() << " (training:" << trainTime << "s, verify: " << verifyTime << "s)" << endl;
	}

	//To speed up execution, we'll split the sets into smaller pieces.
//	TrainingSet verificationSet = trainingSet.split(10000);
//	testSet = testSet.split(1000);
//	vector<TrainingSet> trainingSets;
//	trainingSets.push_back(trainingSet.split(10000));
//	trainingSets.push_back(trainingSet.split(10000));
//	trainingSets.push_back(trainingSet.split(10000));
//	trainingSets.push_back(trainingSet.split(10000));
//	trainingSets.push_back(trainingSet);
//
//	vector<unsigned int> topology;
//	topology.push_back(784);
//	topology.push_back(30);
//	topology.push_back(10);
//
//	NeuralNet neuralNet(topology);
//	unsigned int correct = 0;
//
//	correct = verifyOutput(testSet, neuralNet);
//	cout << "Epoch 0: " << correct << "/" << testSet.size() << endl;
//
//	/* To better see if our neural net is learning, we'll only do one epoch at a time before
//	 * checking against the training set. So we'll do 30 epochs, but in a for-loop instead of
//	 * through the train() method.
//	 */
//
//	for(unsigned int i = 1; i <= 30; ++i) {
//		neuralNet.train(trainingSets.at(i % trainingSets.size()), 1, 10, 3.0);
//		correct = verifyOutput(testSet, neuralNet);
//		cout << "Epoch " << i << ": " << correct << "/" << testSet.size() << endl;
//	}

	/*
	cout.setf(ios::fixed);
	cout.precision(0);

	bool exit = false;
	int i = 0;
	char dump;

	do {
		Activation sample = trainingSet.getInput(i);
		for(int y = 0; y < 28; ++y) {
			for(int x = 0; x < 28; ++x) {
				cout.width(2);
				cout.fill('0');
				cout << sample((y * 28) + x) * 100 << " ";
			}
			cout << endl;
		}
		cout << "Label: " << trainingSet.getOutput(i).maxIndex() << endl;
		cin >> dump;
		++i;
	} while(!exit);
	*/
	return 0;
}
