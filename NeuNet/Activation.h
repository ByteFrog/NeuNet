/*
 * Activation.h
 *
 *  Created on: Sep. 18, 2020
 *      Author: Matthew Virdaeus
 *
 *  Represents an input/output to a neural network.
 */

#pragma once

#include <eigen3/Eigen/Eigen>

class Activation {
	friend class NeuralNet;
public:
	Activation(unsigned int size);
	Activation(const Eigen::VectorXf& vector);
	virtual ~Activation();

	unsigned int size();
	float& operator()(unsigned int row);
	float operator()(unsigned int row) const;
	float maxValue() const;
	unsigned int maxIndex() const;

private:
	Eigen::VectorXf vector_;
};

