/*
 * Activation.cpp
 *
 *  Created on: Sep. 18, 2020
 *      Author: Matthew Virdaeus
 */

#include "Activation.h"

using Eigen::VectorXf;

Activation::Activation(unsigned int size)
: vector_(VectorXf::Zero(size)) {
}

Activation::Activation(const Eigen::VectorXf &vector)
: vector_(vector) {
}

Activation::~Activation() {

}

unsigned int Activation::size() {
	return vector_.rows();
}

float& Activation::operator ()(unsigned int row) {
	return vector_(row);
}

float Activation::operator ()(unsigned int row) const {
	return vector_(row);
}

/* Returns the greatest value in the activation set.
 */
float Activation::maxValue() const {

	return vector_(maxIndex());
}

/* Returns the index of the greatest value in the activation set.
 */
unsigned int Activation::maxIndex() const {

	unsigned int maxPos = 0;
	for(unsigned int i = 1; i < vector_.rows(); ++i) {
		if(vector_(i) > vector_(maxPos)) {
			maxPos = i;
		}
	}
	return maxPos;
}
