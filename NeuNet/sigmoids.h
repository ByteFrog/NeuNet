/*
 * sigmoids.h
 *
 *  Created on: Sep. 18, 2020
 *      Author: Matthew Virdaeus
 *
 *  Defines various sigmoid functions and their derivatives for use in neural networks.
 *
 *  It's currently suggested not to use the tanh() function and it's derivative at this time as they
 *  seem to prevent the network from learning for some reason.
 */

#pragma once

#include <ctgmath>

float logistic(float z) {
	return 1.0/(1.0 + expf(-z));
}

float logistic_prime(float z) {
	return logistic(z) * (1 - logistic(z));
}

//tanh(z) is defined by <ctgmath>

float tanh_prime(float z) {
	return 1 - (tanhf(z) * tanhf(z));
}
