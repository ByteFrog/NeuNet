/*
 * TrainingSet.h
 *
 *  Created on: Sep. 23, 2020
 *      Author: Matthew Virdaeus
 *
 *  A set of training data for a NeuralNet object. Can be used to represent both data the neural network
 *  can learn from, or a verification set to evaluate the neural network's ability to complete a task.
 */

#pragma once

#include <utility>
#include <vector>

#include "Activation.h"

class TrainingSet {
public:
	TrainingSet();
	virtual ~TrainingSet();

	void addPair(const Activation& x, const Activation& y);
	unsigned int size() const;
	Activation getInput(unsigned int index) const;
	Activation getOutput(unsigned int index) const;
	TrainingSet split(unsigned int items);

private:
	std::vector<std::pair<Activation, Activation>> trainingPairs_;
};

