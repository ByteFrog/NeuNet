/*
 * gate_generators.h
 *
 *  Created on: Sep. 26, 2020
 *      Author: Matthew Virdaeus
 */

#pragma once

#include <chrono>
#include <random>

#include "TrainingSet.h"

enum class Gate {
	AND,
	OR,
	XOR,
	NAND,
	NOR
};

TrainingSet generateTrainingSet(Gate type, unsigned int pairs) {

	std::default_random_engine generator(std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_int_distribution<int> distribution(0,1);
	TrainingSet set;

	for(unsigned int i = 0; i < pairs; ++i) {

		int x1 = distribution(generator);
		int x2 = distribution(generator);
		int y = 0;

		switch(type) {
		case Gate::AND:
			y = x1 & x2;
			break;
		case Gate::OR:
			y = x1 | x2;
			break;
		case Gate::XOR:
			y = x1 ^ x2;
			break;
		case Gate::NAND:
			y = !(x1 & x2);
			break;
		case Gate::NOR:
			y = !(x1 | x2);
			break;
		}

		Activation in(2);
		in(0) = (float)x1;
		in(1) = (float)x2;

		Activation out(1);
		out(0) = (float)y;

		set.addPair(in, out);
	}

	return set;
}
