# NeuNet

A C++ class representing an artificial neural network, with additional testing projects.

# Motivation

Artifical neural networks (ANNs) have made a big splash in the work of tech recently, and a lot of really fascinating projects have come out of the popularity. Having studied artificial intelligence back in 2003, it seemed worth revisiting and investigating how ANNs work on a much deeper level. What better way to do that then to write a implementation of an ANN completely from scratch?

# Requirements

[Eigen3](http://eigen.tuxfamily.org/index.php?title=Main_Page)

# Credits

This implementation is based on the methods and source code in "Neural Networks and Deep Learning" by Michael Nielsen available online [here](http://neuralnetworksanddeeplearning.com/index.html). His book was a massive help in understanding the theory of how ANNs work.